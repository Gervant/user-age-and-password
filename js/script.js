//Экранирование изпользуют для вставки различных символов в текст, например тех которые не предполагаются
//стандартной клавиатурой или являются эмоджи, так же с его помощью можно помещать в текст кавычки или скобки
//которые при обычном наборе (без экранирования) означали бы закрытие или открытие фрагмента кода.
createNewUser = () => {
    const newUser = {
        userFirstName: prompt("Input your first name"),
        userLastName: prompt("Input your last name"),
        birthday: prompt("Input your date of birth in dd.mm.yyyy format"),

        getAge() {
            let convertDate = this.birthday.split(".").reverse().join("-")
            return Math.floor((new Date() - new Date(convertDate)) / (60*60*24*1000) / 365)
        },

        getLogin() {
            return `${this.firstName.slice(0, 1).toLowerCase()}${this.lastName.toLowerCase()}`
        },

        getPassword() {
            return `${this.firstName.slice(0,1).toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(6)}`
        },

        setFirstName(value) {
            this.userFirstName = value;
        },

        setLastName(value) {
            this.userLastName = value;
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        get: function () {return this.userFirstName},

    });
    Object.defineProperty(newUser, 'lastName', {
        get: function () {return this.userLastName},

    });

    return newUser;
}

const object = createNewUser();
console.log(object)
console.log(object.getPassword())
console.log(object.getLogin())
console.log(object.getAge())
